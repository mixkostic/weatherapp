package com.example.weatherapp.model;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;

public class City implements ClusterItem {
    private long id;
    private String name;
    private String country;
    private Coord coord;
    private WeatherItem weatherItem;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Override
    public LatLng getPosition() {
        return new LatLng(coord.getLat(), coord.getLon());
    }

    @Override
    public String getTitle() {
        return name;
    }

    @Override
    public String getSnippet() {
        return name;
    }

    public Coord getCoord() {
        return coord;
    }

    public void setCoord(Coord coord) {
        this.coord = coord;
    }

    public WeatherItem getWeatherItem() {
        return weatherItem;
    }

    public void setWeatherItem(WeatherItem weatherItem) {
        this.weatherItem = weatherItem;
    }
}
