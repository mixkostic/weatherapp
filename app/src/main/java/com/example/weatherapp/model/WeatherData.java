package com.example.weatherapp.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class WeatherData {
    @SerializedName("list")
    private List<CityWeather> cityWeatherList;

    public List<CityWeather> getCityWeatherList() {
        return cityWeatherList;
    }

    public void setCityWeatherList(List<CityWeather> cityWeatherList) {
        this.cityWeatherList = cityWeatherList;
    }
}
