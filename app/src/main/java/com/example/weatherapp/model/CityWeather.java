package com.example.weatherapp.model;

import com.google.gson.annotations.SerializedName;

public class CityWeather {
    private WeatherItem main;
    @SerializedName("id")
    private int cityId;

    public WeatherItem getMain() {
        return main;
    }

    public void setMain(WeatherItem main) {
        this.main = main;
    }

    public int getCityId() {
        return cityId;
    }

    public void setCityId(int cityId) {
        this.cityId = cityId;
    }
}
