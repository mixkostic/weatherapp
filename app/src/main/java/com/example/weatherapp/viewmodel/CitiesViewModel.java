package com.example.weatherapp.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.weatherapp.data.CitiesRepository;
import com.example.weatherapp.data.WeatherRepository;
import com.example.weatherapp.model.City;
import com.example.weatherapp.model.WeatherData;

import java.util.List;

public class CitiesViewModel extends AndroidViewModel {

    private CitiesRepository citiesRepository;
    private WeatherRepository weatherRepository;

    public CitiesViewModel(@NonNull Application application) {
        super(application);
        citiesRepository = new CitiesRepository();
        weatherRepository = new WeatherRepository();
    }

    public LiveData<List<City>> getCities() {
        MutableLiveData<List<City>> cityLiveData = new MutableLiveData<>();
        List<City> cities = citiesRepository.getCities(getApplication());
        cityLiveData.postValue(cities);
        return cityLiveData;
    }

    public LiveData<WeatherData> getWeather(String cityIds) {
        return weatherRepository.getWeatherData(cityIds);
    }
}
