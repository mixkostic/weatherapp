package com.example.weatherapp;

import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import com.example.weatherapp.clustering.CustomClusterRenderer;
import com.example.weatherapp.clustering.CustomInfoWindowAdapter;
import com.example.weatherapp.model.City;
import com.example.weatherapp.model.CityWeather;
import com.example.weatherapp.viewmodel.CitiesViewModel;
import com.google.android.gms.common.util.CollectionUtils;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterManager;

import java.util.List;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private static final float MIN_ZOOM = 5f;
    private GoogleMap mMap;
    private List<City> cityList;
    private CitiesViewModel citiesViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        citiesViewModel = ViewModelProviders.of(this).get(CitiesViewModel.class);
        citiesViewModel.getCities().observe(this, cities -> {
            cityList = cities;
            startClusterConfig(mMap, cityList);
        });

    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        configureMap(mMap);
        centerGermanyInMap(mMap);

        startClusterConfig(mMap, cityList);
    }

    private void configureMap(GoogleMap googleMap) {
        UiSettings uiSettings = googleMap.getUiSettings();
        uiSettings.setZoomControlsEnabled(true);
        uiSettings.setCompassEnabled(true);
        googleMap.setMinZoomPreference(MIN_ZOOM);
    }

    private void centerGermanyInMap(GoogleMap googleMap) {
        LatLng eisenachTown = new LatLng(50.97, 10.31);//center of Germany, approximately
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(eisenachTown, 5.7f));
    }

    private void startClusterConfig(GoogleMap map, List<City> cityList) {
        if (map != null && !CollectionUtils.isEmpty(cityList)) {
            StringBuilder cityIds = new StringBuilder();
            for(City city : cityList) {
                if (cityIds.length() > 0) cityIds.append(",");
                cityIds.append(city.getId());
            }

            citiesViewModel.getWeather(cityIds.toString()).observe(this, weatherData -> {
                if (weatherData != null && !CollectionUtils.isEmpty(weatherData.getCityWeatherList())) {
                    List<CityWeather> cityWeatherList = weatherData.getCityWeatherList();
                    for (City city : cityList) {
                        for (int i = 0; i< cityWeatherList.size(); i++) {
                            city.setWeatherItem(cityWeatherList.get(i).getMain());
                        }
                    }
                }


            });

            configureClustering(cityList, map);
        }
    }

    private void configureClustering(List<City> cities, GoogleMap googleMap) {
        ClusterManager<City> clusterManager = new ClusterManager<>(this, googleMap);

        clusterManager.setRenderer(new CustomClusterRenderer(this, googleMap, clusterManager));

        googleMap.setInfoWindowAdapter(clusterManager.getMarkerManager());
        CustomInfoWindowAdapter customInfoWindowAdapter = new CustomInfoWindowAdapter(this);

        clusterManager.getMarkerCollection().setOnInfoWindowAdapter(customInfoWindowAdapter);
        clusterManager.getClusterMarkerCollection().setOnInfoWindowAdapter(customInfoWindowAdapter);

        googleMap.setOnCameraIdleListener(clusterManager);
        googleMap.setOnMarkerClickListener(clusterManager);

        for (City city : cities) {
            clusterManager.addItem(city);
        }

        clusterManager.cluster();

    }
}
