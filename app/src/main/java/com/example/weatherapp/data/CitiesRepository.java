package com.example.weatherapp.data;

import android.content.Context;

import com.example.weatherapp.R;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.List;

import com.example.weatherapp.model.City;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class CitiesRepository {

    public List<City> getCities(Context context) {
        InputStream inputStream = context.getResources().openRawResource(R.raw.german_cities);
        ByteArrayOutputStream byteStream;

        try {
            byte[] buffer = new byte[inputStream.available()];
            inputStream.read(buffer);
            byteStream = new ByteArrayOutputStream();
            byteStream.write(buffer);
            byteStream.close();
            inputStream.close();
            Type type = new TypeToken<List<City>>() {}.getType();
            Gson gson = new Gson();
            return gson.fromJson(byteStream.toString(), type);

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }
}
