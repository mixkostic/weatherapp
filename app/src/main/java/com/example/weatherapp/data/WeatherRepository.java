package com.example.weatherapp.data;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.weatherapp.model.WeatherData;
import com.example.weatherapp.retrofit.WeatherApi;
import com.example.weatherapp.retrofit.WeatherApiUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WeatherRepository {
    private WeatherApi weatherApi = WeatherApiUtils.getWeatherApiService();

    public LiveData<WeatherData> getWeatherData(String cityIds) {
        Call<WeatherData> request = weatherApi.getWeatherData(cityIds);

        MutableLiveData<WeatherData> weatherLiveData = new MutableLiveData<>();

        request.enqueue(new Callback<WeatherData>() {
            @Override
            public void onResponse(Call<WeatherData> call, Response<WeatherData> response) {
                weatherLiveData.postValue(response.body());
            }

            @Override
            public void onFailure(Call<WeatherData> call, Throwable t) {
                weatherLiveData.postValue(null);
            }
        });

        return weatherLiveData;
    }
}
