package com.example.weatherapp.clustering;

import com.example.weatherapp.model.City;
import com.google.android.gms.common.util.CollectionUtils;
import com.google.maps.android.clustering.Cluster;

import java.util.Iterator;

public class ClusteringHelper {
    public City getCity(Cluster<City> cluster) {
        City firstCity = null;
        if (cluster != null && !CollectionUtils.isEmpty(cluster.getItems())) {
            Iterator<City> iterator = cluster.getItems().iterator();
            firstCity = iterator.next();
        }

        return firstCity;
    }
}
