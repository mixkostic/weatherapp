package com.example.weatherapp.clustering;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.weatherapp.R;
import com.example.weatherapp.model.*;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import com.google.maps.android.ui.IconGenerator;

public class CustomClusterRenderer extends DefaultClusterRenderer<City> {
    private Context context;
    private ClusteringHelper clusteringHelper;
    public CustomClusterRenderer(Context context, GoogleMap map, ClusterManager<City> clusterManager) {
        super(context, map, clusterManager);
        this.context = context.getApplicationContext();
        clusteringHelper = new ClusteringHelper();
    }

    @Override
    protected boolean shouldRenderAsCluster(Cluster<City> cluster) {
        return cluster.getSize() > 1;
    }

    @Override
    protected void onBeforeClusterRendered(Cluster<City> cluster, MarkerOptions markerOptions) {
        City firstCity = clusteringHelper.getCity(cluster);
        if (firstCity != null)
            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(getMarkerIcon(firstCity.getName())));

    }

    @Override
    protected void onBeforeClusterItemRendered(City item, MarkerOptions markerOptions) {
        markerOptions.icon(BitmapDescriptorFactory.fromBitmap(getMarkerIcon(item.getName())));
    }

    @Override
    protected void onClusterItemRendered(City clusterItem, Marker marker) {
        marker.setTag(clusterItem);
        super.onClusterItemRendered(clusterItem, marker);
    }

    @Override
    protected void onClusterRendered(Cluster<City> cluster, Marker marker) {
        City firstCity = clusteringHelper.getCity(cluster);
        if (firstCity != null) marker.setTag(firstCity);
        super.onClusterRendered(cluster, marker);
    }

    private Bitmap getMarkerIcon(String text) {
        IconGenerator iconFactory = new IconGenerator(context);

        LayoutInflater myInflater = LayoutInflater.from(context);
        View customView = myInflater.inflate(R.layout.custom_marker, null, false);
        iconFactory.setContentView(customView);

        TextView cityName = customView.findViewById(R.id.city_name);
        cityName.setText(text);

        ImageView icon = customView.findViewById(R.id.weather_icon);
        icon.setBackgroundColor(Color.BLUE);


        return iconFactory.makeIcon();
    }
}
