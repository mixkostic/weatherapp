package com.example.weatherapp.clustering;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.example.weatherapp.R;
import com.example.weatherapp.model.City;
import com.example.weatherapp.model.WeatherItem;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

public class CustomInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {
    private LayoutInflater inflater;

    public CustomInfoWindowAdapter(Context context) {
        inflater = LayoutInflater.from(context);
    }

    @Override
    public View getInfoWindow(Marker marker) {
        View customView = inflater.inflate(R.layout.custom_info_window, null, false);

        TextView titleTextView = customView.findViewById(R.id.title);
        TextView weatherTextView = customView.findViewById(R.id.weatherData);

        Object tag = marker.getTag();
        if (tag != null) {
            City city = (City) tag;
            titleTextView.setText(city.getName());
            WeatherItem weatherItem = city.getWeatherItem();
            if (weatherItem != null) {
                weatherTextView.setText(String.format("temp_min:%s temp_max:%s humidity:%s pressure:%s"
                        , weatherItem.getTemp_min() ,weatherItem.getTemp_max()
                        , weatherItem.getHumidity(), weatherItem.getPressure()));
            } else {
                weatherTextView.setText("No info");
            }

        } else {
            titleTextView.setText("No info");
            weatherTextView.setText("No info");
        }

        return customView;
    }

    @Override
    public View getInfoContents(Marker marker) {
        return null;
    }
}
