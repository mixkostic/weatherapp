package com.example.weatherapp.retrofit;

import com.example.weatherapp.model.WeatherData;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface WeatherApi {

    @GET("group")
    Call<WeatherData> getWeatherData(@Query("id") String cityIds);
}
