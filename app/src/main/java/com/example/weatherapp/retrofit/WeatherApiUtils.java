package com.example.weatherapp.retrofit;

public class WeatherApiUtils {
    static final String KEY_QUERY_PARAMETER_NAME = "APPID";
    static final String KEY_VALUE = "8550d29bff1b56da7a1aeebbc78d6c42";
    static final String UNITS_KEY = "units";
    static final String UNITS_VALUE = "metric";

    public static WeatherApi getWeatherApiService() {
        return RetrofitClient.getRetrofitInstance().create(WeatherApi.class);
    }
}
