package com.example.weatherapp;

import com.example.weatherapp.clustering.ClusteringHelper;
import com.example.weatherapp.model.City;
import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.Cluster;

import org.junit.Test;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNull;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ClusteringHelperTest {
    private City city1 = new City();
    private City city2 = new City();

    private ClusteringHelper clusteringHelper = new ClusteringHelper();

    private Cluster<City> createCluster() {

        return new Cluster<City>() {
            List<City> cityList = getCityList();
            @Override
            public LatLng getPosition() {
                return null;
            }

            @Override
            public Collection<City> getItems() {
                return cityList;
            }

            @Override
            public int getSize() {
                return cityList.size();
            }
        };

    }

    private List<City> getCityList() {
        List<City> cityList = new ArrayList<>();
        city1.setId(1);
        city1.setName("Berlin");

        city2.setId(2);
        city2.setName("Bremen");

        cityList.add(city1);
        cityList.add(city2);

        return cityList;

    }

    @Test
    public void testGetCity_nullCheck() {
        City result = clusteringHelper.getCity(null);
        assertNull(result);
    }

    @Test
    public void testGetCity_emptyCheck() {
        Cluster<City> cluster = createCluster();
        cluster.getItems().clear();
        City city = clusteringHelper.getCity(cluster);
        assertNull(city);
    }

    @Test
    public void testGetCity() {
        Cluster<City> cluster = createCluster();
        City city = clusteringHelper.getCity(cluster);
        assertEquals(city.getId(), city1.getId());
    }
}
